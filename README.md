### build the buildpack and publish (if desired)
```

pack buildpack package \
harbor.proto.tsfrt.net/build-image/cpp-cnb \
 --config ./package.toml -v --publish
```


### run a build with pack
```

pack build test-app8 \
--path cmake-examples/07-package-management/D-conan/i-basic/ \
--buildpack harbor.proto.tsfrt.net/build-image/cpp-cnb \
--env executable_name=third_party_include

```

